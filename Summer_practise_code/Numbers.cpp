// @file Numbers.cpp
#define _CRT_SECURE_NO_WARNINGS
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include "Numbers.h"

int Armstrong_check(int number) {
	int origlNum, remainder, count = 0;
	double result = 0.0;
	origlNum = number;
	while (origlNum != 0) {
		origlNum /= 10;
		++count;
	}
	origlNum = number;
	while (origlNum != 0) {
		remainder = origlNum % 10;
		result += pow(remainder, count);
		origlNum /= 10;
	}
	if ((int)result == number) {
		count = 0;
		result = 0;
		return 1;
	}
	return 0;
}

void Armstrong_range(int low, int high) {
	int return_funck = 0;
	for (low; low < high + 1; ++low) {
		return_funck = Armstrong_check(low);
		if (return_funck == 1) {
			printf("\'%d\' ", low);
		}
		else {
			continue;
		}
	}
}

void chose_option(int chose) {
	int flag = 0;
	char buffer[256];
	printf("\nPRESS \'1\' TO RESTART PROGRAMM:\n");
	printf("PRESS \'0\' TO END:\n");
label:
	printf("-> ");
	while (true) {
		flag = 1;
		scanf("%s", buffer);
		for (int i = 0; i < strlen(buffer); i++) {
			if (!isdigit(buffer[i])) {
				printf("\nINCORECT INPUT!!! TRY AGAIN!!!\n");
				printf("-> ");
				flag = 0;
				break;
			}
		}
		if (flag) {
			chose = atoi(buffer);
			break;
		}
	}
	switch (chose) {
	case 0:
		system("cls");
		printf("THANK YOU FOR WORKING!!!\n");
		exit(0);
		break;
	case 1: break;
	default:
		printf("\nINCORECT INPUT!!! TRY AGAIN!!!\n");
		goto label;
	}
}