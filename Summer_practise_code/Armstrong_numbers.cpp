//@file Armstrong_numbers.cpp
#define _CRT_SECURE_NO_WARNINGS
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include "Numbers.h"

int main() {
	setlocale(LC_ALL, "Rus");
	printf(
		"+---------------------------------------------------------------------"
		"-----------------+\n");
	printf(
		"|  ARMSTRONG NUMBERS IN INPUTED RANGE                              "
		"                 |\n");
	printf(
		"|  DISCRIPTION: This program shows all Armstrong numbers in inputed "
		"range.          |\n");
	printf(
		"|  FORMULA: abcd = a^n + b^n + c^n + d^n, where \'n\' quantity of "
		"digits in number.      |\n");
	printf(
		"+---------------------------------------------------------------------"
		"-----------------+\n");
	while (true) {
		int low = 0, high = 0, chose = 0;
		int flag = 0;
		char buffer[256];
		printf("\nENTER RANGE:\n");

		printf("FROM: ");
		while (true) {
			flag = 1;
			scanf("%s", buffer);
			for (int i = 0; i < strlen(buffer); i++) {
				if (!isdigit(buffer[i])) {
					printf("INCORECT INPUT!!! TRY AGAIN!!!\n");
					printf("-> ");
					flag = 0;
					break;
				}
			}
			if (flag) {
				low = atoi(buffer);
				break;
			}
		}
		printf("TO: ");
		while (true) {
			flag = 1;
			scanf("%s", buffer);
			for (int i = 0; i < strlen(buffer); i++) {
				if (!isdigit(buffer[i])) {
					printf("\nINCORECT INPUT!!! TRY AGAIN!!!\n");
					printf("-> ");
					flag = 0;
					break;
				}
			}
			if (flag) {
				high = atoi(buffer);
				break;
			}
		}
		if (low >= high) {
			printf("\nINCORRECT RANGE!!! TRY AGAIN!!!\n");
			continue;
		}
		printf("ARMSTRONG NUMBERS BETWEEN \'%d\' AND \'%d\' ARE: ", low, high);
		Armstrong_range(low, high);
		printf("\n");
		chose_option(chose);
	}
}